from django.db import models

# Create your models here.
class MTask(models.Model):
    task_name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="pending")
    date_created = models.DateTimeField(auto_now_add=True)
    user_id = models.PositiveIntegerField()

class MEvent(models.Model):
    event_name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="Ongoing")
    event_date = models.DateTimeField()
    user_id = models.PositiveIntegerField()
