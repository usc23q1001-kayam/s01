from django.urls import path
from . import views

# Syntax
# path(route, views, name)

urlpatterns = [
    path('', views.index, name='index'),
    path('add_task', views.add_task, name='add_task'),
    path('add_event', views.add_event, name='add_event'),
    path('profile', views.profile, name='profile'),
    path('login', views.login_v, name='login_v'),
    path('register', views.register_v, name='register_v'),
    path('logout', views.logout_v, name='logout_v'),
    path('todo/<int:todoitem_id>/', views.todoitem, name='todoitem'),
    path('event/<int:eventitem_id>/', views.eventitem, name='eventitem'),
]