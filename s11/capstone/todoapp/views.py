from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

from .models import MTask, MEvent
# Create your views here.
def index(request):
    if request.user.id != None:
        todolist = MTask.objects.filter(user_id=request.user.id)
        eventlist = MEvent.objects.filter(user_id=request.user.id)
        print(todolist)
        print(eventlist)
        context = {
            'todolist': todolist,
            'eventlist': eventlist,
            'user' : request.user
        }
    else:
        context = {
            'user' : request.user
        }
    return render(request, "capstone/index.html", context)

def add_task(request):
    if request.method == 'POST':
        task_name = request.POST['tname']
        description = request.POST['desc']
        user_id = request.user.id
        
        task = MTask(task_name=task_name, description=description, user_id=user_id)
        task.save()
        
        return redirect('index')
    
    context = {
        'user' : request.user
    }
    return render(request, "capstone/add_task.html", context)

def add_event(request):
    if request.method == 'POST':
        event_name = request.POST['ename']
        description = request.POST['desc']
        event_date = request.POST['date']
        user_id = request.user.id
        
        event = MEvent(event_name=event_name, description=description, event_date=event_date, user_id=user_id)
        event.save()
        
        return redirect('index')

    context = {
        'user' : request.user
    }
    return render(request, "capstone/add_event.html", context)

def profile(request):
    
    context = {
        'user' : request.user
    }
    return render(request, "capstone/profile.html", context)

def login_v(request):
    failed = "Username or Password is Invalid"
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        print(user)
        
        if user is not None:
            login(request, user)
            return redirect('index')
        else:
            context = {
                "failed" : failed,
                'user' : request.user
            }
            return render(request, "capstone/login.html", context)
    context = {
        'user' : request.user
    }
    return render(request, "capstone/login.html", context)

def register_v(request):
    users = User.objects.all()
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        cpassword = request.POST['cpassword']
        
        for user in users:      # checks if user is in the db
            if user.username == username:
                context = {
                    'user' : request.user,
                }
                return render(request, "capstone/register.html", context)
            
        if password == cpassword:
            user = User()
            user.username = username
            user.email = email
            user.password = password
            user.set_password(password)
            user.save()
            login_v(request)
        else:
            context = {
                'user' : request.user,
            }
    context = {
        'user' : request.user,
    }
    return render(request, "capstone/register.html", context)

def logout_v(request):
    logout(request)
    return redirect('index')

def todoitem(request, todoitem_id):
    todoitem = MTask.objects.get(id=todoitem_id)
    print(todoitem)
    context = {
        'user' : request.user,
        'todoitem' : todoitem
    }
    return render(request, 'capstone/todoitem.html', context)

def eventitem(request, eventitem_id):
    eventitem = MEvent.objects.get(id=eventitem_id)
    print(eventitem)
    context = {
        'user' : request.user,
        'eventitem' : eventitem
    }
    return render(request, 'capstone/eventitem.html', context)
