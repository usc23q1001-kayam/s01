class Camper:
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type

    def career_track(self):
        print(f"Currently enrolled in the {self.course_type} program of USC.")

    def info(self):
        print(f"My name is {self.name} of batch {self.batch}.")


zuitt_camper = Camper("Kristian Ayam", "1", "Information Technology")

print(zuitt_camper.name)
print(zuitt_camper.batch)
print(zuitt_camper.course_type)

print("")
zuitt_camper.info()
zuitt_camper.career_track()