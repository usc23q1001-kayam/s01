from django.urls import path
from . import views

# Syntax
# path(route, views, name)

urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login_v, name='login'),
    path('reg', views.reg, name='reg'),
    path('change_pass', views.change_pass, name='change_pass'),
    path('logout_v', views.logout_v, name='logout_v'),
]