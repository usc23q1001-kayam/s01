from django.apps import AppConfig


class S09ActConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 's09act'
