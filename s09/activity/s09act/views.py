from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

def index(request):
	context = {
		'user': request.user
	}
	return render(request, "s09act/index.html", context)

def reg(request):
	users = User.objects.all()
	is_reg = False          # default false for verification

	for user in users:      # checks if user is in the db
		if user.username == "krisyam":
			is_reg = True
			break           # stops checking if user is already registered

	if is_reg == False:     # hardcoded registration
		user = User()
		user.username = "krisyam"
		user.email = "kristian.ayam@gmail.com"
		user.password = "password"
		user.set_password("password")
		user.save()
		context = {
			"username": user.username
		}

	context = { "is_reg": is_reg }
	return render(request, "s09act/reg.html", context)


def change_pass(request):
	is_verified = False     # default false for verification
	user = authenticate(username="krisyam", password="password")
	print(user)

	if user is not None:    # if not empty and user is authenticated
		authenticated_user = User.objects.get(username='krisyam')
		authenticated_user.password("changedpassword")
		authenticated_user.save()
		is_verified = True

	context = {
		"is_verified": is_verified
	}

	return render(request, "s09act/change_pass.html", context)

def login_v(request):
	username = "krisyam"
	password = "password"
	user = authenticate(username=username, password=password)
	print(user)

	if user is not None:    # if user is authenticated
		login(request, user)
		return redirect("index")

	else:
		return render(request, "s09act/login.html", context)

def logout_v(request):
	logout(request)
	return redirect("index")
