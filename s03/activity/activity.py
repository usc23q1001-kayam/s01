year = int(input("Enter a year: "))

if year != 0:
    if year % 4 == 0 and year % 100!= 0 or year % 400 == 0:
        print(f"{year} is a leap year")
    else:
        print(f"{year} is not a leap year")
else:
    print("Error, input not a valid year")

x = int(input("Enter number of columns: "))
y = int(input("Enter number of rows: "))

for i in range(y):
    for j in range(x):
      print("*",end=" ")
    print()