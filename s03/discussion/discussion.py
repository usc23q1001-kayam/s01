num1 = int(input("Enter number: "))

if num1 % 3 == 0:
    if num1 % 5 == 0:
        print(num1, "is a multiple of 3 and 5")
    else:
        print(num1, "is a multiple of 3")
elif num1 % 5 == 0:
    print(num1, "is a multiple of 5")
else:
    print(num1, "is not a multiple of 3 or 5")