"""
"I am < name (string)> , and I am < age (integer)> years old, I work as a 
< occupation(string)> , and my rating for < movie (string)> 
is < rating (decimal)> %"


Get the product of num1 and num2
Check if num1 is less than num3
Add the value of num3 to num2
"""

name = "Kristian Benzene Ayam"
age = 20
occupation = "Student"
movie = "Avatar: The Way of Water"
rating = "100.1"

print("I am {}, and I am {} years old, I am a {}, and my rating for {} is {}%\n".format(name, age, occupation, movie, rating))

num1 = 10
num2 = 20
num3 = 30

product = num1 * num2
isLess = num1 < num3
add = num3 + num2


print("The product of {} and {} is {}".format(num1, num2, product))
print("Is {} is less than {}? {}".format(num1, num3, isLess))
print("The addition of {} and {} is {}".format(num1, num2, add))
