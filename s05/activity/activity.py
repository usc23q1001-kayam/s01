class Animal:
    name = ""
    breed = ""
    age = ""


    def make_sound(self):
        pass

    def eat(self, food):
        print(f"{self._name} is eating {food}.")
        
    def call(self):
        print(f"You call your pet... {self._name} comes running to you.")
    
    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_breed(self):
        return self._breed

    def set_breed(self, breed):
        self._breed = breed

    def get_age(self):
        return self._age

    def set_age(self, age):
        self._age = age

class Cat(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
    
    def make_sound(self):
        print(f"{self._name} says meow.")

class Dog(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def make_sound(self):
        print(f"{self._name} says woof.")

dog1 = Dog("Fido", "Labrador Retriever", 6)
dog1.make_sound()
dog1.call()
dog1.eat("Expensive Dog Food")

print("\n\n")

cat1 = Cat("Cathy", "Orange Tabby", 9)
cat1.call()
cat1.eat("Cod")
cat1.make_sound()
