



class Person:
    
    _firstName = ""
    _lastName = ""
    _email = ""
    _department = ""

    def addRequest(self):
        return "Request has been added"
    def checkRequest():
        pass
    def addUser():
        pass
    def addMember():
        pass

    def login(self):
        return self._email + " has logged in"
    def logout(self):
        return self._email + " has logged out"
    
    def getFullName(self):
        return self._firstName + " " + self._lastName
    def getLastName(self):
        return self._lastName
    def getFirstName(self):
        return self._firstName
    def getEmail(self):
        return self._email
    def getDepartment(self):
        return self._department
    
    def setLastName(self, lastName):
        self.lastName = lastName
    def setFirstName(self, firstName):
        self.firstName = firstName
    def setEmail(self, email):
        self.email = email
    def setDepartment(self, department):
        self.department = department


class employee(Person):
    def __init__(self,firstName,lastName,email,department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department


class teamLead(Person):
    def __init__(self,firstName,lastName,email,department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    _members = []

    def addMember(self, a):
        self._members.append(a)

    def getMembers(self):
        return self._members
        
class admin(Person):
    def __init__(self,firstName,lastName,email,department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def addUser(self):
        return "User has been added"


class request():
    name = ""
    requester = ""
    dateRequested = ""
    status = "open"

    def __init__(self,name,requester,dateRequested):
        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested

    def getName(self):
        return self.name
    def getRequester(self):
        return self.requester
    def getDateRequested(self):
        return self.dateRequested
    def getStatus(self):
        return self.status
    
    def setName(self, name):
        self.name = name
    def setRequester(self, requester):
        self.requester = requester
    def setDateRequested(self, dateRequested):
        self.dateRequested = dateRequested
    def setStatus(self, status):
        self.status = status

    def updateRequest(self):
        print(f"Request {self.name} has been updated.")

    def closeRequest(self):
        print(f"Request {self.name} has been closed.")

    def cancelRequest(self):
        print(f"Request {self.name} has been cancelled.")
    

#Test Cases:
employee1 = employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = teamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"


teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.getMembers():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.setStatus("closed")
print(req2.closeRequest())