from django.urls import path
from . import views

# Syntax
# path(route, views, name)

urlpatterns = [
    path('', views.index, name='index'),
]