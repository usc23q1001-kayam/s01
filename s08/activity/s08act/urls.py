from django.urls import path
from . import views

# Syntax
# path(route, views, name)

urlpatterns = [
    path('', views.index, name='index'),
	#/s08act/<groceryitem_id>
	#/s08act/1
	# The <int:groceryitem_id> allows for creating a dynamic link where the groceryitem_id is provided
	path('<int:groceryitem_id>/', views.groceryitem, name='viewgroceryitem'),
]