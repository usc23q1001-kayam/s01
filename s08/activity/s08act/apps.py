from django.apps import AppConfig


class S08ActConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 's08act'
