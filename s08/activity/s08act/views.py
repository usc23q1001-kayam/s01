from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

from .models import GroceryItem

def index(request):
    groceryitem_list = GroceryItem.objects.all()
    context = {'groceryitem_list': groceryitem_list}
    return render(request, "s08act/index.html", context)

	# template = loader.get_template("s08act/index.html")
	# context = {
	# 	'groceryitem_list': groceryitem_list
	# }
	# return HttpResponse(template.render(context, request))

	# output = ', '.join([groceryitem.task_name for groceryitem in groceryitem_list])
	# return HttpResponse(output)
	# return HttpResponse("Hello from the views.py file")

def groceryitem(request, groceryitem_id):
	response = "You are viewing the details of %s"
	return HttpResponse(response % groceryitem_id)